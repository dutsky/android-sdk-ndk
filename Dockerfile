FROM openjdk:8

ENV ANDROID_SDK_TOOLS="4333796"
ENV ANDROID_SDK1="22"
# 2020-03-31
# ENV ANDROID_SDK2="26"
ENV ANDROID_SDK2="29"
ENV ANDROID_SDK_BUILD_TOOLS1="23.0.3"
# 2020-03-31
# 2021-06-16
ENV ANDROID_SDK3="30"
# 2021-06-16
ENV ANDROID_SDK_BUILD_TOOLS2="28.0.3"
ENV ANDROID_SDK_BUILD_TOOLS3="29.0.2"
ENV ANDROID_NDK="r10e"

ENV ANDROID_HOME="/android/sdk"
ENV ANDROID_NDK_HOME="${ANDROID_HOME}/android-ndk-r10e"

RUN apt-get update && apt-get -y install \
	file \
	make \
	gcc-multilib \
	lib32z1 \
	lib32stdc++6 \
&& rm -rf /var/lib/apt/lists/*

RUN mkdir -p ${ANDROID_HOME} \
&& wget --quiet --output-document=${ANDROID_HOME}/android_sdk.zip https://dl.google.com/android/repository/sdk-tools-linux-${ANDROID_SDK_TOOLS}.zip \
&& unzip -q ${ANDROID_HOME}/android_sdk.zip -d ${ANDROID_HOME} \
&& rm ${ANDROID_HOME}/android_sdk.zip

RUN mkdir ${HOME}/.android \
&& touch ${HOME}/.android/repositories.cfg

RUN yes | ${ANDROID_HOME}/tools/bin/sdkmanager "platform-tools" \
&& yes | ${ANDROID_HOME}/tools/bin/sdkmanager "platforms;android-${ANDROID_SDK1}" \
&& yes | ${ANDROID_HOME}/tools/bin/sdkmanager "platforms;android-${ANDROID_SDK2}" \
&& yes | ${ANDROID_HOME}/tools/bin/sdkmanager "platforms;android-${ANDROID_SDK3}" \
&& yes | ${ANDROID_HOME}/tools/bin/sdkmanager "build-tools;${ANDROID_SDK_BUILD_TOOLS1}" \
&& yes | ${ANDROID_HOME}/tools/bin/sdkmanager "build-tools;${ANDROID_SDK_BUILD_TOOLS2}" \
&& yes | ${ANDROID_HOME}/tools/bin/sdkmanager "build-tools;${ANDROID_SDK_BUILD_TOOLS3}" \
&& yes | ${ANDROID_HOME}/tools/bin/sdkmanager --licenses > /dev/null

RUN wget --quiet --output-document=${ANDROID_HOME}/android_ndk.zip https://dl.google.com/android/repository/android-ndk-${ANDROID_NDK}-linux-x86_64.zip \
&& unzip -q ${ANDROID_HOME}/android_ndk.zip -d ${ANDROID_HOME} \
&& rm ${ANDROID_HOME}/android_ndk.zip


